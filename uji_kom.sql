-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 30 Mar 2019 pada 08.15
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uji_kom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris_d` int(11) NOT NULL,
  `kode_peminjaman` varchar(50) NOT NULL,
  `jumlah_p` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris_d`, `kode_peminjaman`, `jumlah_p`, `id_peminjaman`, `id_petugas`) VALUES
(33, 1, 'PJM-4051317813', 1, 34, 5),
(34, 3, 'PJM-4051317813', 1, 35, 5),
(35, 3, 'PJM-3213616844', 1, 36, 5),
(36, 1, 'PJM-3213616844', 1, 37, 5),
(37, 1, 'PJM-4895986788', 1, 38, 2),
(38, 3, 'PJM-4895986788', 1, 39, 0),
(39, 3, 'PJM-7865965256', 1, 40, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam_p`
--

CREATE TABLE `detail_pinjam_p` (
  `id_detail_pinjam_p` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `kode_peminjaman_d_p` varchar(50) NOT NULL,
  `jumlah_p_p` int(11) NOT NULL,
  `id_peminjaman_p` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam_p`
--

INSERT INTO `detail_pinjam_p` (`id_detail_pinjam_p`, `id_inventaris`, `kode_peminjaman_d_p`, `jumlah_p_p`, `id_peminjaman_p`, `id_pegawai`) VALUES
(1, 1, 'PJM-9378042697', 1, 2, 10),
(2, 3, 'PJM-9378042697', 1, 3, 10),
(3, 8, 'PJM-5618041117', 1, 4, 10),
(4, 1, 'PJM-4957777874', 1, 5, 8),
(6, 3, 'PJM-8198816410', 1, 7, 8),
(8, 1, 'PJM-5997038078', 2, 9, 8),
(9, 1, 'PJM-5997038078', 2, 10, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `kondisi` varchar(25) NOT NULL,
  `keterangan` tinytext NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(25) NOT NULL,
  `id_sarana` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_sarana`, `id_petugas`, `foto`) VALUES
(1, 'Laptop', 'Baik', '-', 53, 1, '2019-01-09', 1, '675764', 1, 2, 'Inaho.jpg'),
(3, 'trtfg', 'Baik', '-', 54, 1, '2019-01-25', 1, 'sadasf', 1, 5, 'Mobile-Suit-Gundam-Iron-Blooded-Orphans-Season-2-Header-7-12.jpg'),
(8, 'asdfgh', 'Baik', '-', 49, 1, '2019-02-20', 1, '09312', 1, 5, '2dfa66df7a3391266c6688fc5fa0eb06.jpg'),
(9, 'Infokus', 'Baik', 'INI INFOKUS', 10, 1, '2019-03-07', 1, '2412312312', 1, 5, 'a.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(25) NOT NULL,
  `kode_jenis` varchar(25) NOT NULL,
  `keterangan_j` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan_j`) VALUES
(1, 'Elektronik', '78658', 'Elektronik'),
(2, 'KBM', '68566', '-'),
(4, 'Olahraga', '68567', 'BOLA KU Yang Keren'),
(9, 'IMPACTER', '18638', 'TOOLS'),
(11, 'Agent', '007', 'Alatnya James Bond Tapi Boong hehehe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'Administrator'),
(2, 'Petugas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(25) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(8, 'Hijikata', '870238412341232', 'INDONESIA SELATAN BAGIAN UJUNGIH'),
(10, 'Okita', '870238412341234', 'INDONESIA SELATAN BAGIAN UJUNGIH');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `kode_peminjaman_p` varchar(50) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `kode_peminjaman_p`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_inventaris`, `id_petugas`) VALUES
(34, 'PJM-4051317813', '2019-03-30', '0000-00-00', 'Pinjam', 1, 5),
(35, 'PJM-4051317813', '2019-03-30', '2019-03-30', 'Kembali', 3, 5),
(36, 'PJM-3213616844', '2019-03-30', '0000-00-00', 'Pinjam', 3, 5),
(37, 'PJM-3213616844', '2019-03-30', '0000-00-00', 'Pinjam', 1, 5),
(38, 'PJM-4895986788', '2019-03-30', '0000-00-00', 'Pinjam', 1, 2),
(39, 'PJM-4895986788', '2019-03-30', '0000-00-00', 'Pinjam', 3, 0),
(40, 'PJM-7865965256', '2019-03-30', '2019-03-30', 'Kembali', 3, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman_p`
--

CREATE TABLE `peminjaman_p` (
  `id_peminjaman_p` int(11) NOT NULL,
  `kode_peminjaman_pe` varchar(50) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman_pe` varchar(50) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman_p`
--

INSERT INTO `peminjaman_p` (`id_peminjaman_p`, `kode_peminjaman_pe`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman_pe`, `id_inventaris`, `id_pegawai`) VALUES
(2, 'PJM-9378042697', '2019-03-09', '2019-03-09', 'Kembali', 1, 10),
(3, 'PJM-9378042697', '2019-03-09', '2019-03-09', 'Kembali', 3, 10),
(4, 'PJM-5618041117', '2019-03-09', '2019-03-09', 'Kembali', 8, 10),
(5, 'PJM-4957777874', '2019-03-30', '0000-00-00', 'Pinjam', 1, 8),
(7, 'PJM-8198816410', '2019-03-30', '0000-00-00', 'Pinjam', 3, 8),
(9, 'PJM-5997038078', '2019-03-30', '2019-03-30', 'Kembali', 1, 8),
(10, 'PJM-5997038078', '2019-03-30', '2019-03-30', 'Kembali', 1, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_petugas` varchar(25) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `id_level`) VALUES
(2, 'reza', '526e34d04735124f05a090181f3e6e8a', 'Reza Setiawan', 2),
(4, 'scathach', 'bebe', 'king', 1),
(5, 'raja', '526e34d04735124f05a090181f3e6e8a', 'Mohamad Raja Fakhri', 1),
(9, 'Stella', 'nobel', 'Arash', 2),
(10, 'tele', '526e34d04735124f05a090181f3e6e8a', 'tele', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `request`
--

CREATE TABLE `request` (
  `id_request` int(11) NOT NULL,
  `nama_alat` varchar(20) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal_request` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `keterangan` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(25) NOT NULL,
  `kode_ruang` varchar(25) NOT NULL,
  `keterangan_r` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan_r`) VALUES
(1, 'Lab RPL 1', '231454', 'Testing'),
(2, 'asdsa1234', '12341', 'asfasdASFASD'),
(11, 'Lab Animasi 1', '33311', 'asfasdasfsad');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sarana`
--

CREATE TABLE `sarana` (
  `id_sarana` int(11) NOT NULL,
  `nama_sarana` varchar(50) NOT NULL,
  `kode_sarana` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sarana`
--

INSERT INTO `sarana` (`id_sarana`, `nama_sarana`, `kode_sarana`) VALUES
(1, 'Rekayasa Perangkat Lunak', '001'),
(2, 'Animasi', '002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `nis` char(15) NOT NULL,
  `alamat` text NOT NULL,
  `jk` varchar(20) NOT NULL,
  `no_hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nama_siswa`, `nis`, `alamat`, `jk`, `no_hp`) VALUES
(1, 'Fathurohman Nurasyam', '123412512342123', 'Pintu Ledeng Ke Dalam', 'Laki - Laki', '089779797989');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `detail_pinjam_ibfk_1` (`id_inventaris_d`);

--
-- Indexes for table `detail_pinjam_p`
--
ALTER TABLE `detail_pinjam_p`
  ADD PRIMARY KEY (`id_detail_pinjam_p`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `inventaris_ibfk_4` (`id_sarana`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `peminjaman_ibfk_1` (`id_petugas`);

--
-- Indexes for table `peminjaman_p`
--
ALTER TABLE `peminjaman_p`
  ADD PRIMARY KEY (`id_peminjaman_p`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `petugas_ibfk_1` (`id_level`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id_request`),
  ADD KEY `request_ibfk_1` (`id_petugas`),
  ADD KEY `request_ibfk_2` (`id_ruang`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `sarana`
--
ALTER TABLE `sarana`
  ADD PRIMARY KEY (`id_sarana`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `detail_pinjam_p`
--
ALTER TABLE `detail_pinjam_p`
  MODIFY `id_detail_pinjam_p` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `peminjaman_p`
--
ALTER TABLE `peminjaman_p`
  MODIFY `id_peminjaman_p` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id_request` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sarana`
--
ALTER TABLE `sarana`
  MODIFY `id_sarana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `detail_pinjam_ibfk_3` FOREIGN KEY (`id_inventaris_d`) REFERENCES `inventaris` (`id_inventaris`);

--
-- Ketidakleluasaan untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `request`
--
ALTER TABLE `request`
  ADD CONSTRAINT `request_ibfk_1` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `request_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
