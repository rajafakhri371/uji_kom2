<?php
    $kode_pjm=$_GET['kode_pjm'];
?>
<div class="card-area">
              <div class="row">
            <?php
            foreach($db->detail($_GET['id_sarana']) as $d){
            ?>
            <div class="col-lg-3 col-md-3 mt-3">
                <div class="card card-bordered">
                    <img class="card-img-top img-fluid" src="assets/images/inventaris/<?php echo $d['foto'] ?>" style="height: 150px;" alt="image">
                    <div class="card-body">
                        <h5 class="title"><?php echo $d['nama'] ?></h5>
                        <p class="card-text">
                            <h5 class="title">Jumlah : <?php echo $d['jumlah'] ?></h5>
                        </p>
                        <?php

                        $sql=$konek->query("SELECT * FROM detail_pinjam_p WHERE kode_peminjaman_d_p='$kode_pjm' AND id_inventaris='$d[id_inventaris]'");
                        $cek = $sql->num_rows;

                        $data = $sql->fetch_array();

                        if ($cek == 1) {
                            echo "<a href='#' class='btn btn-danger'>Telah Di Pinjam</a>";
                        }else{

                        ?>
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg-<?=$d['id_inventaris'];?>">Pinjam</a>
                    <?php } ?>
                    </div>
                </div>      
            </div>

             <div class="modal fade bd-example-modal-lg-<?=$d['id_inventaris'];?>">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Anda Akan Meminjam Alat : <br><?php echo $d['nama'] ?></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <h2 align="center">Peminjaman</h2>
                            <form action="function/proses.php?aksi=pinjam_p" method="POST">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="id" id="example-text-input" value=" <?php echo $_SESSION['Pegawai'] ?>" required="">
                                    <label for="example-text-input" class="col-form-label">Nama Peminjaman</label>               
                                    <input class="form-control" type="text" name="nama_peminjaman" maxlength="25" id="example-text-input" value="<?php echo $_SESSION['nama_pegawai'] ?>" required="" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Inventaris</label>
                                    <input class="form-control" type="text" name="id_inventaris" id="example-text-input" value="<?php echo $d['id_inventaris'] ?>" readonly>
                                    <input class="form-control" type="hidden" name="id_sarana" id="example-text-input" value="<?php echo $d['id_sarana'] ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Kode Peminjaman</label>
                                    <input class="form-control" type="text" name="kode_pjm" id="example-text-input" value="<?php echo $kode_pjm ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Nama Barang</label>
                                    <input class="form-control" type="text" name="nama_inventaris" maxlength="25" id="example-text-input" value="<?php echo $d['nama'] ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Jumlah Pinjam Barang</label>
                                    <input class="form-control" type="number" name="jumlah" id="example-text-input" max="<?php echo $d['jumlah'] ?>" min="1" value="1" required="" >
                                </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Pinjam</button>
                        </div>
                        </form>
                    </div>
                </div>
        </div>

            <?php
            }
            ?>


</div>
    </div>
        <br>
    <div class="card">
            <div class="card-body">
                    <div class="row">
                    <input class="form-control col-sm-3" type="text" style="height: 44px;" maxlength="25" id="example-text-input" value="<?php echo $kode_pjm ?>" required="" readonly>                             

        <div class="col-lg-12 mt-1">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Peminjaman</h4>
                                <div class="single-table">
                                    <div class="table-responsive">
                                        <table class="table text-center">
                                            <thead class="text-uppercase bg-primary">
                                                <tr class="text-white">
                                                    <th scope="col">No</th>                                                    
                                                    <th scope="col">Nama Barang</th>
                                                    <th scope="col">Jumlah Pinjam</th>
                                                    <th scope="col">OPSI</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              <?php
                                              $no=1;
                                              $total = 0;
                                              foreach ($db->detail_p_b_p($kode_pjm,$d['id_inventaris']) as $d) {
                                                $total += $d['jumlah_p_p'];
                                                if ($d['jumlah_p_p']==0) {
                                                    $del=$konek->query("DELETE FROM detail_pinjam_p WHERE kode_peminjaman_d_p='$kode_pjm' AND id_inventaris='$d[id_inventaris]'");
                                                    $del_p = $konek->query("DELETE FROM peminjaman_p WHERE kode_peminjaman_pe='$kode_pjm' AND id_inventaris='$d[id_inventaris]'");
                                                }else{
                                                ?>
                                                <tr>
                                                    <th scope="row"><?php echo $no++; ?></th>
                                                    <td><?php echo $d['nama']; ?></td>
                                                    <td><?php echo $d['jumlah_p_p']; ?></td>
                                                    <td><a href="#" data-toggle="modal" data-target="#exampleModalLong-<?=$d['id_inventaris'];?>"><i class="btn btn-success ti-plus"></i></a>
                                                    <a href="#" data-toggle="modal" data-target="#exampleModalLong-kurangi-<?=$d['id_inventaris'];?>"><i class="btn btn-danger ti-minus"></i></a>
                                                    <a href="function/proses.php?aksi=hapus_p_p&id_peminjaman=<?=$d['id_peminjaman_p'];?>&id_sarana=<?=$_GET['id_sarana'];?>&id_detail_p=<?=$d['id_detail_pinjam_p']?>&id_inventaris=<?=$d['id_inventaris']?>&kode_pjm=<?=$kode_pjm?>&jumlah_p=<?=$d['jumlah_p_p']?>" ><i class="btn btn-danger ti-trash"></i></a>
                                                </td>
                                                </tr>
                                                <?php
                                                }

                                                ?>

              <div class="modal fade" id="exampleModalLong-<?=$d['id_inventaris'];?>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Tambah Barang Pinjam : <br><?php echo $d['nama'] ?></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form action="function/proses.php?aksi=tam_bar_p_p" method="POST">
                                <div class="form-group">
                                  <input class="form-control" type="hidden" name="kode_pjm" id="example-text-input" value="<?php echo $kode_pjm ?>" readonly>
                                  <input class="form-control" type="hidden" name="id_inventaris" id="example-text-input" value="<?php echo $d['id_inventaris'] ?>" readonly>
                                  <input class="form-control" type="hidden" name="id_sarana" id="example-text-input" value="<?php echo $d['id_sarana'] ?>" readonly>
                                    <label for="example-text-input" class="col-form-label">Nama Barang</label>
                                    <input class="form-control" type="text" name="nama_inventaris" maxlength="25" id="example-text-input" value="<?php echo $d['nama'] ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Jumlah Pinjam Barang</label>
                                    <input class="form-control" type="number" name="jumlah" id="example-text-input" max="<?php echo $d['jumlah'] ?>" min="1" value="1" required="" >
                                </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Pinjam</button>
                        </div>
                        </form>
                    </div>
                </div>
        </div>

        <div class="modal fade" id="exampleModalLong-kurangi-<?=$d['id_inventaris'];?>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Kurangi Barang Pinjam : <br><?php echo $d['nama'] ?></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form action="function/proses.php?aksi=kurangin_p" method="POST">
                                <div class="form-group">
                                  <input class="form-control" type="hidden" name="kode_pjm" id="example-text-input" value="<?php echo $kode_pjm ?>" readonly>
                                  <input class="form-control" type="hidden" name="id_inventaris" id="example-text-input" value="<?php echo $d['id_inventaris'] ?>" readonly>
                                  <input class="form-control" type="hidden" name="id_sarana" id="example-text-input" value="<?php echo $d['id_sarana'] ?>" readonly>
                                    <label for="example-text-input" class="col-form-label">Nama Barang</label>
                                    <input class="form-control" type="text" name="nama_inventaris" maxlength="25" id="example-text-input" value="<?php echo $d['nama'] ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Jumlah Pinjam Barang</label>
                                    <input class="form-control" type="number" name="jumlah" id="example-text-input" max="<?php echo $d['jumlah_p_p'] ?>" min="1" value="<?php echo $d['jumlah_p_p'] ?>" required="" >
                                </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Pinjam</button>
                        </div>
                        </form>
                    </div>
                </div>
        </div>
         <?php 
         }
        ?>
        <tr>
        <th colspan="3" style="text-align: right">Total Peminjaman Barang</th>
        <td><input type="text" class="form-control" name="total" value="<?=$total?>" readonly></td>
        </tr>
        </tbody>
        </table>
        </div>
        </div>
        </div>
          </div>
        </div>
        </div>
        </div>

                    
      </div>
