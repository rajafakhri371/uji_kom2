<?php
$kode_pjm = $_GET['kode_peminjaman'];
$nama_pegawai = $_GET['nama_pegawai'];
?>
<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Detail Peminjaman</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <div class="row">
                            <div class="col-lg-2">
                            <span><input type="text" class="form-control" style="width: 10rem;" value="<?php echo $kode_pjm; ?>" readonly></span>
                            </div>
                            <div class="col-lg-2">
                            <span><input type="text" class="form-control" style="width: 10rem;" value="<?php echo $nama_pegawai; ?>" readonly></span>
                            </div>
                            </div>
                            <br>
                            <br></a></span>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Inventaris</th>
                                        <th scope="col">Jumlah Yang Di Pinjam</th>
                                        <th scope="col">Tanggal Pinjam</th>                                  
                                        <th scope="col">Tanggal Kembali</th>                                  
                                        <th scope="col">Status</th>                                  
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table2_detail_p($kode_pjm) as $tb){
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $tb['nama']; ?></td>
                                    <td><?php echo $tb['jumlah_p_p']; ?></td>
                                    <td><?php echo date('d F Y', strtotime($tb['tanggal_pinjam'])) ?></td>
                                    <?php
                                    if ($tb['tanggal_kembali'] == 0000-00-00) {
                                        echo"<td> - </td>";
                                    }else{

                                    ?>
                                    <td><?php echo date('d F Y', strtotime($tb['tanggal_kembali'])) ?></td>
                                <?php  }?>
                                    <td><?php echo $tb['status_peminjaman_pe']; ?></td>
                                    <td>
                                        <?php
                                        if ($tb['status_peminjaman_pe'] == 'Kembali') {
                                            echo"Telah Kembali";
                                        }else{
                                        ?>
                                        <a href="function/proses.php?aksi=kembali_b_p&id_peminjaman_p=<?=$tb['id_peminjaman_p']?>&jumlah=<?=$tb['jumlah_p_p']?>&id_inventaris=<?=$tb['id_inventaris']?>&kode_pjm=<?=$kode_pjm?>&nama_pegawai=<?=$nama_pegawai?>"><i class="btn btn-success ti-arrow-left"></i></a>
                                        <?php
                                    }

                                         ?>

                                    </td>
                                </tr>

<!--             <div class="modal fade" id="exampleModalLong-<?=$tb['id_peminjaman']?>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Anda Akan Mengembalikan Alat : <br><?php echo $d['nama'] ?></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form action="function/proses.php?aksi=kembali_b" method="POST">
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Jumlah Kembalikan Barang</label>
                                    <input class="form-control" type="hidden" name="id_peminjaman" id="example-text-input" value="<?=$tb['id_peminjaman']?>" required="" >
                                    <input class="form-control" type="hidden" name="id_detail_pinjam" id="example-text-input" value="<?=$tb['id_detail_pinjam']?>" required="" >
                                    <input class="form-control" type="hidden" name="kode_pjm" id="example-text-input" value="<?=$kode_pjm?>" required="" >
                                    <input class="form-control" type="hidden" name="nama_petugas" id="example-text-input" value="<?=$nama_petugas?>" required="" >
                                    <input class="form-control" type="number" name="jumlah" id="example-text-input" max="<?php echo $tb['jumlah_p'] ?>" min="<?php echo $tb['jumlah_p'] ?>" value="1" required="" >
                                </div>            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Kembalikan</button>
                        </div>
                        </form>
                    </div>
                </div>
        </div> -->


                                <?php
                            }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>