<?php
   $id_petugas = $_GET['id_petugas'];
   $nama_petugas = $_GET['nama_petugas'];
?>
<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Peminjaman Petugas</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <div class="row">
                            <div class="col-lg-2">
                            <span><input type="text" class="form-control" style="width: 10rem;" value="<?php echo $nama_petugas; ?>" readonly></span>
                            </div>
                            </div>                            
                            <br>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Kode Peminjaman</th>
                                        <th scope="col">Jumlah Yang Di Pinjam</th>
                                        <th scope="col">Tanggal Pinjam</th>                                  
                                        <th scope="col">Status</th>                                  
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table1_adm($id_petugas) as $tb){
                                    
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?=$tb['kode_peminjaman_p']?></td>
                                    <td><?php 
                                    $k=$tb['kode_peminjaman_p'];
                                         $sum=$konek->query("SELECT SUM(jumlah_p) as total FROM detail_pinjam WHERE kode_peminjaman='$k'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total'];
                                          }
                                    ?></td>
                                    <td><?php echo date('d F Y', strtotime($tb['tanggal_pinjam'])); ?></td>
                                    <td><?php 
                                    $k=$tb['kode_peminjaman_p'];
                                         $sql=$konek->query("SELECT status_peminjaman FROM peminjaman WHERE kode_peminjaman_p='$k' AND status_peminjaman='Pinjam'");
                                         $cek = $sql->num_rows;
                                         $data = $sql->fetch_array();
                                         if ($cek >= 1) {
                                             echo"Pinjam";
                                         }else{
                                            echo "Kembali";
                                         }
                                          
                                    ?></td>                                    
                                    <td>
                                        <a href="?page=pengembalian_operator&opsi=tabel_view_op&kode_peminjaman=<?php echo $tb['kode_peminjaman']; ?>&nama_petugas=<?php echo $tb['nama_petugas']; ?>"><i class="btn btn-primary ti-eye"></i></a>    
                                    </td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                </div>
                    </div>
                </div>
        </div>
        </div>
</div>