<?php
foreach($db->edit_jenis($_GET['id_jenis']) as $d){
?>

<div class="col-lg-6 col-ml-12">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h3>Edit Data Jenis</h3>
                    <p class="text-muted font-14 mb-4">Pastikan Untuk Mengisi Data Dengan Benar</p>
                    <form action="function/proses.php?aksi=update_jenis" method="POST">
                    <div class="form-group">
                        <input class="form-control" type="hidden" name="id_jenis" id="example-text-input" required="" value="<?php echo $d['id_jenis']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Nama Jenis</label>
                        <input class="form-control" type="text" name="nama_jenis" maxlength="25" id="example-text-input" required="" value="<?php echo $d['nama_jenis']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Kode Jenis</label>
                        <input class="form-control" type="text" name="kode_jenis" maxlength="5" id="example-text-input" required="" value="<?php echo $d['kode_jenis']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Keterangan</label>
                        <textarea name="keterangan" class="form-control" id="" cols="30" rows="10"><?php echo $d['keterangan']; ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4"> Simpan</button>
                    <?php } ?>
                    </form>
                </div>
            </div>
        </div>
</div>