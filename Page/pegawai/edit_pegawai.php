<?php
foreach($db->edit_pegawai($_GET['id_pegawai']) as $d){
?>

<div class="col-lg-6 col-ml-12">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h3>Edit Data Ruangan</h3>
                    <p class="text-muted font-14 mb-4">Pastikan Untuk Mengisi Data Dengan Benar</p>
                    <form action="function/proses.php?aksi=update_pegawai" method="POST">
                    <div class="form-group">
                        <input class="form-control" type="hidden" name="id_pegawai" id="example-text-input" required="" value="<?php echo $d['id_pegawai']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Nama Petugas</label>
                        <input class="form-control" type="text" name="nama_pegawai" maxlength="25" id="example-text-input" required="" value="<?php echo $d['nama_pegawai'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Username</label>
                        <input class="form-control" type="text" name="username" maxlength="25" id="example-text-input" required="" value="<?php echo $d['username'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Password</label>
                        <input class="form-control" type="Password" name="password" maxlength="25" id="example-text-input" required="" value="<?php echo $d['password'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">NIP</label>
                        <input class="form-control" type="text" name="nip" maxlength="15" pattern="[0-9]+" id="example-number-input" required="" value="<?php echo $d['nip']?> ">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Alamat</label>
                         <textarea name="alamat" class="form-control" id="" cols="30" rows="10"><?php echo $d['alamat'] ?></textarea>
                         <input class="form-control" type="hidden" name="level" id="example-text-input" required="" value="3">
                    </div>
                    <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4"> Simpan</button>
                    <?php } ?>
                    </form>
                </div>
            </div>
        </div>
</div>