<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Pegawai</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <span><a href="#" data-toggle="modal" data-target="#tambah_pegawai"><i class="fa fa-plus-square" style="font-size:210%;"></i></a></span>
                            <br>
                            <br></a></span>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Pegawai</th>
                                        <th scope="col">Username</th>                                  
                                        <th scope="col">NIP</th>                                  
                                        <th scope="col">Alamat</th>                                  
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->tampil_data_pegawai() as $js){
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $js['nama_pegawai']; ?></td>
                                    <td><?php echo $js['username']; ?></td>
                                    <td><?php echo $js['nip']; ?></td>
                                    <td><?php echo $js['alamat']; ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#edit_pegawai-<?php echo $js['id_pegawai']; ?>"><i class="ti-pencil-alt"></i></a> ||
                                        <a href="function/proses.php?id_pegawai=<?php echo $js['id_pegawai']; ?>&aksi=hapus_pegawai"><i class="ti-trash"></i></a>          
                                    </td>
                                </tr>

                                <!-- Modal -->
                                <div class="modal fade" id="edit_pegawai-<?php echo $js['id_pegawai']; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Data Pegawai</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <form action="function/proses.php?aksi=update_pegawai" method="POST">
                                                                <div class="form-group">
                                                                    <input class="form-control" type="hidden" name="id_pegawai" id="example-text-input" required="" value="<?php echo $js['id_pegawai']; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">Nama Petugas</label>
                                                                    <input class="form-control" type="text" name="nama_pegawai" maxlength="25" id="example-text-input" required="" value="<?php echo $js['nama_pegawai'] ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">Username</label>
                                                                    <input class="form-control" type="text" name="username" maxlength="25" id="example-text-input" required="" value="<?php echo $js['username'] ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">Password</label>
                                                                    <input class="form-control" type="Password" name="password" maxlength="25" id="example-text-input" required="" value="<?php echo $js['password'] ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">NIP</label>
                                                                    <input class="form-control" type="text" name="nip" maxlength="15" pattern="[0-9]+" id="example-number-input" required="" value="<?php echo $js['nip']?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">Alamat</label>
                                                                     <textarea name="alamat" class="form-control" id="" cols="30" rows="10"><?php echo $js['alamat'] ?></textarea>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                        </div>
                        </div>

                                <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- Modal -->
                <div class="modal fade" id="tambah_pegawai">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Data Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            </div>
                            <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="function/proses.php?aksi=tambah_pegawai" method="POST">
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Nama Pegawai</label>
                                        <input class="form-control" type="text" name="nama_pegawai" maxlength="25" id="example-text-input" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Username</label>
                                        <input class="form-control" type="text" name="username" maxlength="25" id="example-text-input" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Password</label>
                                        <input class="form-control" type="Password" name="password" maxlength="25" id="example-text-input" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">NIP</label>
                                        <input class="form-control" type="text" name="nip" maxlength="15" pattern="[0-9]+" id="example-number-input" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Alamat</label>
                                         <textarea name="alamat" class="form-control" id="" cols="30" rows="10"></textarea>
                                         <input class="form-control" type="hidden" name="level" id="example-text-input" required="" value="3">
                                    </div>

                                    <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4"> Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
                    </div>
                </div>
        </div>
        </div>
</div>