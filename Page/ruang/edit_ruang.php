<?php
foreach($db->edit_ruang($_GET['id_ruang']) as $d){
?>

<div class="col-lg-6 col-ml-12">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h3>Edit Data Ruangan</h3>
                    <p class="text-muted font-14 mb-4">Pastikan Untuk Mengisi Data Dengan Benar</p>
                    <form action="function/proses.php?aksi=update_ruang" method="POST">
                    <div class="form-group">
                        <input class="form-control" type="hidden" name="id_ruang" id="example-text-input" required="" value="<?php echo $d['id_ruang']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Nama Ruangan</label>
                        <input class="form-control" type="text" name="nama_ruang" maxlength="25" id="example-text-input" required="" value="<?php echo $d['nama_ruang']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Kode Ruangan</label>
                        <input class="form-control" type="text" name="kode_ruang" maxlength="5" id="example-text-input" required="" value="<?php echo $d['kode_ruang']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Keterangan</label>
                        <textarea name="keterangan" class="form-control" id="" cols="30" rows="10"><?php echo $d['keterangan']; ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4"> Simpan</button>
                    <?php } ?>
                    </form>
                </div>
            </div>
        </div>
</div>