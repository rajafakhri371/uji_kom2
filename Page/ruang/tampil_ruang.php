<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Ruangan</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                        	<span><a href="#" data-toggle="modal" data-target="#tambah_ruang"><i class="fa fa-plus-square" style="font-size:210%;"></i></a></span>
                            <br>
                            <br>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Ruangan</th>
                                        <th scope="col">Kode Ruangan</th>
                                        <th scope="col">Keterangan</th>
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
								$no = 1;
								foreach($db->tampil_data_ruang() as $x){
								?>
								<tr>
									<th scope="row"><?php echo $no++; ?></th>
									<td><?php echo $x['nama_ruang']; ?></td>
									<td><?php echo $x['kode_ruang']; ?></td>
									<td><?php echo $x['keterangan_r']; ?></td>
									<td>
										<a href="#" data-toggle="modal" data-target="#edit_ruang-<?php echo $x['id_ruang']; ?>"><i class="ti-pencil-alt"></i></a> ||
										<a href="function/proses.php?id_ruang=<?php echo $x['id_ruang']; ?>&aksi=hapus_ruang"><i class="ti-trash"></i></a>			
									</td>
								</tr>

                            <!-- Modal -->
                                <div class="modal fade" id="edit_ruang-<?php echo $x['id_ruang']; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Data Ruangan</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                        <div class="row">

                                        <div class="col-lg-6 col-ml-12">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <form action="function/proses.php?aksi=update_ruang" method="POST">
                                                        <div class="form-group">
                                                            <input class="form-control" type="hidden" name="id_ruang" id="example-text-input" required="" value="<?php echo $x['id_ruang']; ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="example-text-input" class="col-form-label">Nama Ruangan</label>
                                                            <input class="form-control" type="text" name="nama_ruang" maxlength="25" id="example-text-input" required="" value="<?php echo $x['nama_ruang']; ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="example-text-input" class="col-form-label">Kode Ruangan</label>
                                                            <input class="form-control" type="text" name="kode_ruang" maxlength="5" id="example-text-input" required="" value="<?php echo $x['kode_ruang']; ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="example-text-input" class="col-form-label">Keterangan</label>
                                                            <textarea name="keterangan" class="form-control" id="" cols="30" rows="10"><?php echo $x['keterangan_r']; ?></textarea>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                            <?php 
                                }
                            ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
                <div class="modal fade" id="tambah_ruang">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Data Ruangan</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            </div>
                            <div class="modal-body">
                        <div class="row">

                        <div class="col-lg-6 col-ml-12">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <form action="function/proses.php?aksi=tambah_ruang" method="POST">
                                            <div class="form-group">
                                                <label for="example-text-input" class="col-form-label">Nama Ruangan</label>
                                                <input class="form-control" type="text" name="nama_ruang" maxlength="25" id="example-text-input" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="example-text-input" class="col-form-label">Kode Ruangan</label>
                                                <input class="form-control" type="text" name="kode_ruang" maxlength="5" id="example-text-input" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="example-text-input" class="col-form-label">Keterangan</label>
                                                <textarea name="keterangan" class="form-control" id="" cols="30" rows="10"></textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>

    </div>