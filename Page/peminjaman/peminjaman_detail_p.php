<div class="card-area">
        <div class="row">
            <?php
            foreach($db->detail($_GET['id_jurusan']) as $d){
            ?>
            <div class="col-lg-4 col-md-6 mt-5">
                <div class="card card-bordered">
                    <img class="card-img-top img-fluid" src="assets/images/inventaris/<?php echo $d['foto'] ?>" style="height: 300px;" alt="image">
                    <div class="card-body">
                        <h5 class="title"><?php echo $d['nama'] ?></h5>
                        <p class="card-text">
                        </p>
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg-<?=$d['id_inventaris'];?>">Go More....</a>
                    </div>
                </div>      
            </div>
             <div class="modal fade bd-example-modal-lg-<?=$d['id_inventaris'];?>">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Anda Akan Meminjam Alat : <br><?php echo $d['nama'] ?></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <h2 align="center">Peminjaman</h2>
                            <form action="function/proses.php?aksi=pinjam_1" method="POST">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="id" id="example-text-input" value=" <?php echo $_SESSION['Petugas'] ?>" required="">
                                    <label for="example-text-input" class="col-form-label">Nama Peminjaman</label>               
                                    <input class="form-control" type="text" name="nama_peminjaman" maxlength="25" id="example-text-input" value="<?php echo $_SESSION['nama_petugas'] ?>" required="" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Inventaris</label>
                                    <input class="form-control" type="text" name="id_inventaris" id="example-text-input" value="<?php echo $d['id_inventaris'] ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Nama Barang</label>
                                    <input class="form-control" type="text" name="nama_inventaris" maxlength="25" id="example-text-input" value="<?php echo $d['nama'] ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-form-label">Jumlah Pinjam Barang</label>
                                    <input class="form-control" type="number" name="jumlah" id="example-text-input" max="<?php echo $d['jumlah'] ?>" min="1" value="1" required="" >
                                </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Pinjam</button>
                        </div>
                        </form>
                    </div>
                </div>
        </div>
            <?php
            }
            ?>


</div>
</div>